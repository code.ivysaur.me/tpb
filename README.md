# tpb

![](https://img.shields.io/badge/written%20in-PHP-blue)

A web interface for a certain type of magnet link dump.

Supports reading the compressed file without any further processing (at the expense of slightly longer search times)

Tags: torrent


## Download

- [⬇️ tpb-0.1.php.rar](dist-archive/tpb-0.1.php.rar) *(1.53 KiB)*
